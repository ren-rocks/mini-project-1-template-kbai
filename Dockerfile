FROM continuumio/miniconda3

WORKDIR /opt/conda

COPY environment.yml .
RUN echo "creating conda environment" \
    && conda env create -f environment.yml

SHELL ["conda", "run", "-n", "KBAI", "/bin/bash", "-c"]

RUN python -c "import PIL"

VOLUME /etc/app
WORKDIR /etc/app

ENTRYPOINT ["conda", "run", "-n", "KBAI", "python", "main.py"]
